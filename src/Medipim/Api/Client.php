<?php

namespace Medipim\Api;

use Medipim\Api\Common\RequestDto;
use Medipim\Api\Exceptions\MedipimApiError;

class Client
{
    /** @var int */
    protected $keyId;
    /** @var string */
    protected $keySecret;
    /** @var string */
    protected $baseUrl;

    /**
     * @param int $apiKeyId
     * @param string $apiKeySecret
     * @param string $baseUrl
     */
    public function __construct(int $apiKeyId, string $apiKeySecret, string $baseUrl)
    {
        $this->keyId = $apiKeyId;
        $this->keySecret = $apiKeySecret;
        $this->baseUrl = $baseUrl;
    }

    /**
     * @param string $path
     * @param array|null $query
     * @return array
     * @throws MedipimApiError
     */
    public function get($path, array $query = null)
    {
        $request = new RequestDto("GET", $path);
        $request->query = $query;
        return $this->request($request);
    }

    /**
     * @param string $path
     * @param array|null $body
     * @return array|\Iterator The response body, or an iterator of results if the response is streaming
     * @throws MedipimApiError
     */
    public function post($path, array $body)
    {
        $request = new RequestDto("POST", $path);
        $request->bodyJson = $body;
        return $this->request($request);
    }

    /**
     * @param $path
     * @param array $query
     * @param callable $callback
     */
    public function stream($path, array $query = [], callable $callback)
    {
        $request = new RequestDto("POST", $path);

        $url = $this->baseUrl . "/" . ltrim($request->path, "/");
        $handle = curl_init($url);

        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_USERPWD, "{$this->keyId}:{$this->keySecret}");
        curl_setopt($handle, CURLOPT_HTTPHEADER, ["Content-type: application/json"]);
        curl_setopt($handle, CURLOPT_POSTFIELDS, json_encode($query));
        curl_setopt($handle, CURLOPT_CUSTOMREQUEST, $request->method);

        $remainder = "";
        curl_setopt($handle, CURLOPT_WRITEFUNCTION, function ($handle, $data) use (&$remainder, &$callback) {
            $lines = explode("\n", $data);
            $lines[0] = $remainder . $lines[0];
            $remainder = array_pop($lines);
            foreach ($lines as $line) {
                call_user_func($callback, json_decode($line, true), $handle);
            }
            return strlen($data);
        });

        curl_exec($handle);

        if ($remainder) {
            $callback(json_decode($remainder, true), $handle);
        }
    }

    /**
     * @param string $path
     * @param string $file
     * @param string|null $mimeType
     * @return array
     */
    public function upload(string $path, string $file, $mimeType = null)
    {
        $request = new RequestDto("POST", $path);
        $request->bodyFile = $file;
        $request->bodyFileType = $mimeType ?: mime_content_type($file);
        return $this->request($request);
    }

    /**
     * @param string $id Product ID
     * @param int $apiVersion
     * @return string
     */
    public function createProductEmbedUrl(string $id, int $apiVersion = 4)
    {
        $timestamp = time() + 90;
        $message = "embed:" . $id . ":" . $timestamp;
        $mac = hash_hmac("sha256", $message, $this->keySecret);

        $query = http_build_query([
            "id" => $id,
            "t" => $timestamp,
            "key" => $this->keyId,
            "mac" => $mac,
        ]);

        return $this->baseUrl . "/v{$apiVersion}/products/embed?" . $query;
    }

    /**
     * @param RequestDto $request
     * @return mixed|\Iterator
     */
    protected function request(RequestDto $request)
    {
        $retries = 0;
        retry:

        $url = $this->baseUrl . "/" . ltrim($request->path, "/");
        if (isset($request->query)) {
            $url .= "?" . http_build_query($request->query);
        }

        $handle = curl_init($url);

        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_USERPWD, "{$this->keyId}:{$this->keySecret}");
        curl_setopt($handle, CURLOPT_CUSTOMREQUEST, $request->method);
        if (isset($request->bodyFile)) {
            curl_setopt($handle, CURLOPT_HTTPHEADER, ["Content-type: " . $request->bodyFileType]);
            curl_setopt($handle, CURLOPT_UPLOAD, true);
            curl_setopt($handle, CURLOPT_INFILE, fopen($request->bodyFile, "rb"));
            curl_setopt($handle, CURLOPT_INFILESIZE, filesize($request->bodyFile));
        } else if (isset($request->bodyJson)) {
            curl_setopt($handle, CURLOPT_HTTPHEADER, ["Content-type: application/json"]);
            curl_setopt($handle, CURLOPT_POSTFIELDS, json_encode($request->bodyJson));
        }

        $this->throttle();

        $rawResponse = curl_exec($handle);
        $status = curl_getinfo($handle, CURLINFO_HTTP_CODE);
        $contentType = curl_getinfo($handle, CURLINFO_CONTENT_TYPE);

        if ($rawResponse === false) {
            throw new MedipimApiError(curl_error($handle));
        }

        if ($status === 429) {
            if ($retries < 3) {
                $retries++;
                sleep($retries);
                goto retry;
            }
        }

        if ($contentType === "application/x-json-stream") {
            return $this->parseStreamResponse($rawResponse);
        }

        return $this->parseResponse($rawResponse);
    }

    /** @var int[] */
    private $requests = [];
    /** @var float */
    private $throttle = 0.0;

    private function throttle()
    {
        $window = 20;
        $rate = 100/60;

        while ($this->requests && $this->requests[0] < time() - $window) {
            array_shift($this->requests);
        }

        if (count($this->requests)/$window > $rate) {
            $this->throttle += 0.05;
        } else if ($this->throttle > 0) {
            $this->throttle = max($this->throttle - 0.02, 0);
        }

        usleep($this->throttle * 1e6);

        $this->requests[] = time();
    }

    private function parseStreamResponse(string $rawResponse)
    {
        $i = 0;
        while ($i < strlen($rawResponse)) {
            $nextNewline = strpos($rawResponse, "\n", $i);
            $line = substr($rawResponse, $i, $nextNewline - $i);
            yield $this->parseResponse($line);
            $i = $nextNewline + 1;
        }
    }

    private function parseResponse(string $rawResponse)
    {
        $response = json_decode($rawResponse, true);

        if (json_last_error()) {
            throw new MedipimApiError("Error parsing response: " . json_last_error_msg());
        }

        if (isset($response["error"])) {
            throw new MedipimApiError(
                $response["error"]["message"] ?: $response["error"]["code"],
                $response["error"]
            );
        }

        return $response;
    }
}
